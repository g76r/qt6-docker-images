#!/bin/bash
set -C
set -e
set -o pipefail

DOCKERFILE="qt6.dockerfile"
IMGS="qt6-builder qt6-runner"
VER_DATE=20241203
VER_QT=6.8.1
#TAG_SUFFIX=debian12
ACCOUNT=g76r

mkdir -p empty
for IMG in $IMGS; do
  if [ -z "$TAG_SUFFIX" ]; then
    TAGS="$ACCOUNT/$IMG:$VER_DATE $ACCOUNT/$IMG:qt-$VER_QT"
  else
    TAGS="$ACCOUNT/$IMG:$VER_DATE-$TAG_SUFFIX $ACCOUNT/$IMG:qt-$VER_QT-$TAG_SUFFIX"
  fi
  for TAG in $TAGS; do
    docker build --build-arg BRANCH=$VER_QT --target $IMG -t $TAG -f $DOCKERFILE empty 
    docker push $TAG
  done
done

TARBALL=qt-$VER_QT-$VER_DATE.tar
rm -f $TARBALL
ID=$(docker create $TAG)
docker cp $ID:/opt/Qt/. - > $TARBALL
docker rm -v $ID
