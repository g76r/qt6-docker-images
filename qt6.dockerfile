FROM debian:12.8-slim AS common
ENV LANG=C.UTF-8
ENV TZ=UTC
RUN apt-get -y update && apt-get -y upgrade && apt-get clean
RUN adduser --disabled-password --gecos "" --uid 1001 user
RUN mkdir /opt/Qt && chown user:user /opt/Qt && ln -sf /opt/Qt/bin/qmake /usr/bin/qmake
RUN apt-get install -y git build-essential cmake ninja-build libssl-dev libmariadb-dev libpqxx-dev unixodbc-dev libsqlite3-dev libpq-dev libpcap-dev graphviz rsync imagemagick zip && apt-get clean
# ccache

FROM common AS temp
ARG BRANCH
RUN apt-get install -y libfontconfig1-dev libfreetype6-dev libx11-dev libx11-xcb-dev libxext-dev libxfixes-dev libxi-dev libxrender-dev libxcb1-dev libxcb-cursor-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-image0-dev libxcb-shm0-dev libxcb-icccm4-dev libxcb-sync-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0-dev libxcb-util-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev libgl-dev libglx-dev libopengl-dev libvulkan-dev
USER user:user
WORKDIR /home/user
RUN git clone --depth 1 --branch $BRANCH git://code.qt.io/qt/qtbase.git
RUN git clone --depth 1 --branch $BRANCH git://code.qt.io/qt/qtsvg.git
WORKDIR /home/user/qtbase
RUN ./configure -prefix /opt/Qt -release -force-debug-info -gif -ico -sql-psql -sql-sqlite -sql-odbc -sql-mysql -qt-sqlite -qt-libpng -qt-libjpeg -fontconfig -system-freetype -qt-harfbuzz -inotify -qt-pcre -openssl-runtime -qt-zlib -opensource -confirm-license -nomake examples -nomake tests
#-no-opengl --ccache -sctp
RUN cmake --build . --parallel
RUN cmake --install .
WORKDIR /home/user/qtsvg
ENV CMAKE_PREFIX_PATH /opt/Qt
RUN cmake .
RUN make
RUN make install

FROM common AS qt6-runner
LABEL org.opencontainers.image.authors="Grégoire Barbier <devel@g76r.eu>"
COPY --from=temp /opt/Qt /opt/Qt
# if gui is needed: libgl1 libglx0 libopengl0 libvulkan1

FROM common AS qt6-builder
LABEL org.opencontainers.image.authors="Grégoire Barbier <devel@g76r.eu>"
COPY --from=temp /opt/Qt /opt/Qt
RUN apt-get install -y awscli docker.io inkscape libgl-dev libglx-dev libopengl-dev libvulkan-dev && apt-get clean
USER user:user
WORKDIR /home/user
RUN git config --global --add safe.directory '*'
